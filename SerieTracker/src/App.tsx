import React, { useContext, useEffect } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './theme/theme.css';

/* Routes */
import { ROUTE_AUTH_BASE, ROUTE_FICHE, ROUTE_HOME, ROUTE_POLICY } from './nav/Routes';
import Home from './pages/Home';

import AppContext from './data/app-context';
import PrivateRoute from './nav/PrivateRoutes';
import AuthRoutes from './nav/AuthRoutes';
import Policy from './pages/Policy';
import Fiche from './pages/FIche';

const App: React.FC = () => {
  const appCtx = useContext(AppContext);

  useEffect(() => {
    appCtx.initContext();
  }, [])

  return (
    <IonApp>
      <IonReactRouter>
        <IonRouterOutlet>
          <Switch>
            <PrivateRoute path={ROUTE_HOME} component={Home} />              
            <PrivateRoute exact path={`${ROUTE_FICHE}:id`} component={Fiche} />
            <Route path={ROUTE_AUTH_BASE} component={AuthRoutes} />
            <Route path={ROUTE_POLICY} component={Policy} />           
            <Redirect path="/" to={ROUTE_HOME} />
          </Switch>
        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
  )
};

export default App;
