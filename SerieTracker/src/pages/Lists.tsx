import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCol,
    IonContent,
    IonFab,
    IonFabButton,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonPage,
    IonRow,
    IonTitle,
    IonToolbar
  } from '@ionic/react';
  import React, { useContext, useState } from 'react';
  
  import { add } from 'ionicons/icons';
  import AppContext from '../data/app-context';
import ResponsiveContent from '../components/ResponsiveContent';
import ContentCard from '../components/ContentCard';
import ContentModal from '../components/AddContentModal'
import SideMenuS from '../nav/SideMenuS';
  
  const Lists: React.FC = () => {
    const [showModal, setShowModal] = useState(false);
    const appCtx = useContext(AppContext);
    return (
      <IonPage>
        <ContentModal showModal={showModal} setShowModal={setShowModal} />
        <IonHeader>
          <IonToolbar>
            <IonTitle>Liste</IonTitle>
            <SideMenuS/>
          </IonToolbar>
        </IonHeader>
        
        
        <IonContent className="ion-padding">
          <IonGrid className="ion-no-padding">
            <IonRow>
              <ResponsiveContent>
                <IonGrid>
                  <IonRow>
                    {
                      appCtx.content.length > 0 ?
                        appCtx.content.map((content, index) => (
                          <IonCol size="12" sizeSm="6" sizeXl="4" key={index}>
                            <ContentCard contentId={content.id} />
                          </IonCol>
                        ))
                        :
                        <h3 className="ion-text-center">
                          Nothing to show yet, add your first content using the button below:
                        </h3>
                    }
                  </IonRow>
                </IonGrid>
              </ResponsiveContent>
            </IonRow>
          </IonGrid>
          <IonFab vertical={appCtx.content.length > 0 ? "bottom" : "center"} horizontal={appCtx.content.length > 0 ? "end" : "center"} slot="fixed">
            <IonFabButton onClick={() => setShowModal(true)}>
              <IonIcon icon={add} />
            </IonFabButton>
          </IonFab>
        </IonContent>
      </IonPage>
    );
  };
  
  export default Lists;
  