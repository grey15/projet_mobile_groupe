import {
    IonAlert,
    IonBackButton,
    IonButton,
    IonButtons,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonNote,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonSlide,
    IonSlides,
    IonTitle,
    IonToolbar
  } from '@ionic/react';
  import { camera, trashOutline } from 'ionicons/icons';
  import React, { useContext, useEffect, useState } from 'react';
  
  import { useHistory, useParams } from 'react-router-dom';
  import ContentInputItems from '../components/ContentInputItems';
  import ContentInputItemsNotes from '../components/ContentInputNotes';
  import AppContext from '../data/app-context';  
  import { ROUTE_HOME } from '../nav/Routes';
  import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
  import ResponsiveContent from '../components/ResponsiveContent';
  import firebase from '../firebase';
  import 'firebase/storage';
import { type } from 'os';
  
  const { Camera } = Plugins;
  
  const Fiche: React.FC = () => {
    const history = useHistory();
    const id = useParams<{ id: string }>().id;
    const [showAlert, setShowAlert] = useState(false);
    const [pictureUrl, setPictureUrl] = useState<string>();
    const appCtx = useContext(AppContext)
  
    const content = appCtx.content.find(content => content.id === id)    
  
    const deleteHandler = () => {
      console.log(content)
      if (content?.id) {
        appCtx.deleteContent(content)
        history.goBack();
      }
    }
  
    const updateTitle = (newTitle: string) => {
      if (!content || !newTitle) return
      let updateContent = { ...content }
      updateContent.title = newTitle;
      appCtx.updateContent(updateContent);
    }
  
    const updatePictureDisplayed = async () => {
      const storage = firebase.storage();
      const storageRef = storage.ref();
      if (!content || content.picture.length < 1) return
      storageRef.child(content.picture[0]).getDownloadURL().then(function (url) {
        console.log(url)
        setPictureUrl(url)
      })
    }
  
    const takePhotoHandler = async () => {
      const photo = await Camera.getPhoto({
        quality: 80,
        resultType: CameraResultType.Base64,
        source: CameraSource.Prompt,
        width: 500,
      });
  
      if (!photo || !photo.base64String || !content || !appCtx.user) return
  
      const fileName = appCtx.user.uid + '/' + content.id + '.jpeg';
  
      const storage = firebase.storage();
      const storageRef = storage.ref();
      const imageRef = storageRef.child(fileName);
      const uploadTask = await imageRef.putString(photo.base64String, 'base64')
      console.log(uploadTask)
  
      let updateContent = { ...content }
      updateContent.picture = [fileName];
      appCtx.updateContent(updateContent)
      updatePictureDisplayed();
    }
  
    useEffect(() => {
      updatePictureDisplayed()
    }, [content?.picture])
  
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot='start'>
              <IonBackButton defaultHref={ROUTE_HOME} />
            </IonButtons>
            
          </IonToolbar>
        </IonHeader>
  
        {content &&
          <IonContent className="ion-padding-bottom" >
            {pictureUrl &&
              <IonSlides pager style={{ backgroundColor: "grey" }}>
                <IonSlide>
                  <IonImg style={{ height: "200px" }} src={pictureUrl} />
                </IonSlide>
              </IonSlides>
            }
  
            <IonGrid>
              <IonRow>
                <ResponsiveContent>
                  <IonGrid>
                    <IonRow className="ion-align-items-center">
                      <IonCol style={{ color: "grey" }}>
                        {content?.picture.length} pictures
                  </IonCol>
                      <IonCol className="ion-text-end">
                        <IonButton onClick={takePhotoHandler} size="small" fill="outline">
                          <IonIcon icon={camera} />
                        </IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                  <IonList className="ion-padding-vertical">                    
                    <IonItem>
                      <IonLabel onClick={() => setShowAlert(true)}>
                        {content?.title}
                      </IonLabel>
                    </IonItem>
                    <IonItem>
                    <IonLabel onClick={() => setShowAlert(true)}>
                        {content?.type}
                      </IonLabel>
                        
                    </IonItem>
                    <IonItem>
                      <IonLabel onClick={() => setShowAlert(true)}>
                        {content?.number1}
                      </IonLabel>
                    </IonItem>
                    <IonItem>
                      <IonLabel onClick={() => setShowAlert(true)}>
                        {content?.number2}
                      </IonLabel>
                    </IonItem>
                    <IonItem>
                      <IonLabel onClick={() => setShowAlert(true)}>
                        {content?.detail}
                      </IonLabel>
                    </IonItem>
                  </IonList>
                  <IonGrid className="ion-margin-top">
                    <IonRow>
                      <IonCol className="ion-text-center">
                        <IonButton fill="outline" size="small" color="danger" onClick={deleteHandler}>
                          <IonIcon icon={trashOutline} slot="icon-only" />
                        </IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </ResponsiveContent>
              </IonRow>
            </IonGrid>
          </IonContent>
        }
  
        <IonAlert
          isOpen={showAlert}
          onDidDismiss={() => setShowAlert(false)}
          header={'Update Content'}
          inputs={[
            {
              name: 'content',
              type: 'text',
              id: 'title-content',
              value: content?.title,
              placeholder: 'Title'
            }
          ]}
          buttons={[
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Confirm Cancel');
              }
            },
            {
              text: 'Ok',
              handler: (inputData) => updateTitle(inputData.title)
            }
          ]}
        />
      </IonPage>
    );
  };
  
  export default Fiche;
  