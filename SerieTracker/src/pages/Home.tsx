import {
  IonCol,
  IonContent,
  IonFab,
  IonFabButton,
  IonGrid,
  IonHeader,
  IonIcon,
  
  IonImg,
  
  IonItem,
  
  IonLabel,
  
  IonList,
  
  IonMenu,
  
  IonPage,
  IonRouterOutlet,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonSlide,
  IonSlides,
  IonThumbnail,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import React, { useContext, useState } from 'react';

import { add } from 'ionicons/icons';
import ContentCard from '../components/ContentCard';
import ContentModal from '../components/AddContentModal';
import AppContext from '../data/app-context';
import ResponsiveContent from '../components/ResponsiveContent';
import SideMenuS from '../nav/SideMenuS';
import { Route, Redirect } from 'react-router';
import { ROUTE_LISTES, ROUTE_PAGES_BASE, ROUTE_HOME } from '../nav/Routes';
import List from './Lists';




const Home:React.FC<{ contentId: string }> = (props) => {
  const [showModal, setShowModal] = useState(false);
  const appCtx = useContext(AppContext);
  const [ContentS, setContentS] = useState(false)  
  const [hairColor, setHairColor] = useState<string>('brown');
 
  return (
    <IonPage>
      <ContentModal showModal={showModal} setShowModal={setShowModal} />
      <IonMenu side="start" menuId="first">
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>Start Menu</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          <IonItem>Menu Item</IonItem>
          <IonItem>Menu Item</IonItem>
          <IonItem>Menu Item</IonItem>
          <IonItem>Menu Item</IonItem>
          <IonItem>Menu Item</IonItem>
        </IonList>
      </IonContent>
    </IonMenu>

    <IonRouterOutlet>
    <Route path={ROUTE_LISTES} component={List} exact />
    <Redirect path={ROUTE_PAGES_BASE} exact to={ROUTE_HOME} />
    </IonRouterOutlet>
      
      
      
      <IonContent className="ion-padding">
        <IonGrid className="ion-no-padding">
          <IonRow>
            <ResponsiveContent>
              <IonGrid>
                <IonRow>                
                  {
                     appCtx.content.length > 0 ?
                      appCtx.content.map((content, index) => (
                        <IonCol size="6" sizeSm="6" sizeXl="4" key={index}>
                          <ContentCard contentId={content.id} />
                        </IonCol>
                      ))
                      :
                      <h3 className="ion-text-center">
                        Nothing to show yet, add your first content using the button below:
                      </h3>
                  }   

                </IonRow>
              </IonGrid>
              
              
              
            </ResponsiveContent>
          </IonRow>
        </IonGrid>
        <IonFab vertical={appCtx.content.length > 0 ? "bottom" : "center"} horizontal={appCtx.content.length > 0 ? "end" : "center"} slot="fixed">
          <IonFabButton onClick={() => setShowModal(true)}>
            <IonIcon icon={add} />
          </IonFabButton>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default Home;
