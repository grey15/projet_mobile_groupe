import { IonAlert, IonItem, IonLabel, IonNote } from '@ionic/react';
import React, { useContext, useState } from 'react';
import AppContext, { Content } from '../data/app-context';


const ContentInputDetail: React.FC<{ content: Content }> = (props) => {
    const appCtx = useContext(AppContext)
    const [showAlert, setShowAlert] = useState(false);

    const update = (data: string) => {
        let updatedContent = { ...props.content }
        updatedContent.detail = data;
        appCtx.updateContent(updatedContent);
    }
    return (
        <IonItem className="ion-margin-bottom">
            <IonLabel onClick={() => setShowAlert(true)}>
                <p>{props.content.detail}</p>
            </IonLabel>
            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                header='Notes'
                inputs={[
                    {
                        name: 'notes',
                        type: 'textarea',
                        id: `content-notes`,
                        value: props.content.detail,
                        placeholder: 'Your notes'
                    }
                ]}
                buttons={[
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: (alertData) => update(alertData['notes'])
                    }
                ]} />
        </IonItem>
    )
}

export default ContentInputDetail