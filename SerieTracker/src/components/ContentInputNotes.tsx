import { IonAlert, IonItem, IonLabel, IonNote } from '@ionic/react';
import React, { useContext, useState } from 'react';
import AppContext, { Content } from '../data/app-context';


const ApartmentInputNotes: React.FC<{ content: Content }> = (props) => {
    const appCtx = useContext(AppContext)
    const [showAlert, setShowAlert] = useState(false);

    const update = (data: string) => {
        let updatedContent = { ...props.content }
        updatedContent.detail = data;
        appCtx.updateContent(updatedContent);
    }
    return (
        <IonItem className="ion-margin-bottom">
            <IonLabel onClick={() => setShowAlert(true)}>
                <p>{props.content.detail}</p>
            </IonLabel>
            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                header='detail'
                inputs={[
                    {
                        name: 'detail',
                        type: 'textarea',
                        id: `content-detail`,
                        value: props.content.detail,
                        placeholder: 'Your detail'
                    }
                ]}
                buttons={[
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: (alertData) => update(alertData['notes'])
                    }
                ]} />
        </IonItem>
    )
}

export default ApartmentInputNotes