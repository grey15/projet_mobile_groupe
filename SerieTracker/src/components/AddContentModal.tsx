import { FilesystemDirectory, Plugins } from '@capacitor/core';
import {
    IonButton,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonModal,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonTextarea,
    IonTitle,
    IonToolbar
} from '@ionic/react';
import { title } from 'process';
import React, { useContext, useEffect, useRef, useState } from 'react';
import AppContext, { Content } from '../data/app-context';
import AddPictureNewContent, { Picture } from './AddPictureNewContent';
import { v4 as uuidv4 } from 'uuid';
import firebase from "../firebase";
import 'firebase/storage';
const { Filesystem } = Plugins;

const AddContentModal: React.FC<{ showModal: boolean, setShowModal: (value: boolean) => void }> = (props) => {
    
    const [title, setTitle] = useState<string>("New title")
    const [detail, setDetail] = useState<string>("New Detail")
    const [type, setType] = useState<string>("New Type")
    const [number1, setNum1] = useState<number>(0)
    const [number2, setNum2] = useState<number>(0)
    
    const appCtx = useContext(AppContext);
    const contentUuid = useRef<string>(uuidv4())
    const [picture, setPicture] = useState<Picture>();

    const [showAlert, setShowAlert] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<string>();
    

    const resetModal = () => {
        setPicture(undefined)
        contentUuid.current = uuidv4();
    }

    const addHandler = async () => {
        if (!appCtx.user?.uid) return
        // Save picture on firebase
        let pictures: string[] = [];
        if (picture && picture.base64) {
            const newPictureName = appCtx.user.uid + '/' + contentUuid.current + '.jpeg';
            const storage = firebase.storage();
            const storageRef = storage.ref();
            const imageRef = storageRef.child(newPictureName);
            await imageRef.putString(picture.base64, 'base64')
            pictures.push(newPictureName)
        }

        let newContent: Content = {            
            id: contentUuid.current,
            userId: appCtx.user?.uid,
            title: title ? title.toString() : "",
            type: type ? type.toString() : "",
            detail: detail ? detail.toString() : "",            
            number1: number1? +number1 : 0,
            number2: number2? +number2 : 0,
            picture: pictures,
        }

        const db = firebase.firestore();
        await db.collection(`Content`)
            .doc(newContent.id)
            .set(newContent)
            .then(() => {
                props.setShowModal(false)
            })
            .catch(error => {
                setErrorMessage(error.message)
                setShowAlert(true)
            });
    }

    const updatePicture = (newPicture: Picture) => {
        setPicture(newPicture)
    }

    return (
        <IonModal isOpen={props.showModal} onDidPresent={resetModal}>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Add new Content</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonGrid>
                    <IonRow>
                        <IonCol></IonCol>
                    </IonRow>
                </IonGrid>
                <IonList className="ion-padding-bottom" mode="ios">                    
                    <IonItem>
                        <IonLabel position="floating">title</IonLabel>
                        <IonInput onIonChange={(e) => {if (e.detail.value) setTitle(e.detail.value)}} value={title}></IonInput>
                    </IonItem>         
                    <IonItem>
                        <IonLabel>type</IonLabel>
                        <IonSelect  value={type} okText="Okay" cancelText="Dismiss" onIonChange={(e) => {if (e.detail.value) setType(e.detail.value)}}>
                            <IonSelectOption value="Movie">Movie</IonSelectOption>
                            <IonSelectOption value="Reading">Reading</IonSelectOption>
                            <IonSelectOption value="Série">Série</IonSelectOption>              
                        </IonSelect>
                    </IonItem>
                     
                     <IonItem>
                        <IonLabel position="floating">number1</IonLabel>                        
                        <IonInput value={number1} onIonChange={(e) => {if (e.detail.value) setNum1(+e.detail.value)}} type="number"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="floating">number2</IonLabel>                        
                        <IonInput value={number2} onIonChange={(e) => {if (e.detail.value) setNum2(+e.detail.value)}} type="number"></IonInput>
                    </IonItem>
                    
                    
                    <IonItem>
                        <IonLabel position="floating">description</IonLabel>
                        <IonInput onIonChange={(e) => {if (e.detail.value) setDetail(e.detail.value)}} value={detail}></IonInput>
                        
                    </IonItem>
                    
                    <AddPictureNewContent updatePicture={updatePicture} />
                </IonList>
                <IonGrid>
                    <IonRow className="ion-justify-content-between">
                        <IonCol size="auto" >
                            <IonButton fill="outline" onClick={() => props.setShowModal(false)}>Cancel</IonButton>
                        </IonCol>
                        <IonCol size="auto" >
                            <IonButton onClick={addHandler}>Save</IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonModal>
    );
};

export default AddContentModal;

