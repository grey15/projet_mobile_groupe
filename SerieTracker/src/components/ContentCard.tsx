import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonGrid, IonImg, IonLabel, IonRow } from '@ionic/react';
import React, { useContext, useEffect, useState } from 'react';
import AppContext from '../data/app-context';
import { ROUTE_FICHE } from '../nav/Routes';
import defaultImg from '../assets/default.png';
import firebase from '../firebase';
import 'firebase/storage';
import './ContentCard.scss'

const ContentCard: React.FC<{ contentId: string }> = (props) => {
  const appCtx = useContext(AppContext);
  const [profileUrl, setProfileUrl] = useState<string>();
  const N1T = "";
  const N2T = "";

  const content = appCtx.content.find(content => content.id === props.contentId)
  

  const updateBase64 = async () => {
    const storage = firebase.storage();
    const storageRef = storage.ref();
    if (!content?.picture || content?.picture.length < 1) return
    storageRef.child(content.picture[0]).getDownloadURL().then(function (url) {
      setProfileUrl(url)
    })
  }

  const setTypeT = async () =>{
    if(content?.type == "Reading"){
      const N1T = "Tome :";
    }
  }

  useEffect(() => {
    updateBase64()
  }, [content?.picture])


  return (
    <React.Fragment>
      {
        content &&
        <IonCard routerLink={ROUTE_FICHE + content?.id}>          
          <IonImg className='IonImgSmall' src={profileUrl ? profileUrl : defaultImg}></IonImg>            
            <IonCardTitle>{content?.title}</IonCardTitle> 
          <IonCardContent>
            <IonLabel>{N1T} </IonLabel>              
            {content?.number1}                    
          </IonCardContent>
          <IonCardContent>
          <IonLabel>Chapitre: </IonLabel>             
            {content?.number2}         
          </IonCardContent>
        </IonCard>
      }
    </React.Fragment>


  );
};

export default ContentCard;
