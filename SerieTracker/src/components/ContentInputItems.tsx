import { IonAlert, IonItem, IonLabel, IonNote } from '@ionic/react';
import React, { useContext, useState } from 'react';
import AppContext, { Content, ContentInputFields } from '../data/app-context';


const ContentInputItems: React.FC<{ field: ContentInputFields, content: Content, friendlyName: string, unit: string }> = (props) => {
    const appCtx = useContext(AppContext)
    const [showAlert, setShowAlert] = useState(false);

    const update = (data: number | string) => {
        let updatedContent = {...props.content }
        if (props.field === "number1" || props.field === "number2"){
            updatedContent[props.field] = +data;
        }else{
            updatedContent[props.field] = String(data);
        }
        
        appCtx.updateContent(updatedContent);
    }
    return (
        <IonItem>
            <IonLabel>
                {props.friendlyName}
            </IonLabel>
            <IonNote onClick={() => setShowAlert(true)} slot="end">{props.content[props.field]}{props.unit}</IonNote>
            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                header={props.friendlyName}
                inputs={[
                    {
                        name: props.field,
                        type: 'text',
                        id: `content-${props.field}`,
                        value: props.content[props.field],
                        placeholder: 'Your ' + props.friendlyName
                    }
                ]}
                buttons={[
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: (alertData) => update(alertData[props.field])
                    }
                ]} />
        </IonItem>
    )
}

export default ContentInputItems