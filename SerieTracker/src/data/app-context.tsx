import userEvent from '@testing-library/user-event';
import React from 'react';
import firebase from '../firebase';

export interface Content {
    id: string,
    title: string,
    detail: string,
    type: string,
    number1: number,
    number2: number,
    picture: string[],
    userId?: string,
}

export interface Profile {
    username: string;
    id: string,
    name: string,
    picture: string[] | null,
    
}

/*
export type FinancialInfoFields = "loanRate" | "insuranceRate" | "loanPeriod" | "notaryFees" | "contribution";
*/

export type ContentInputFields = "title" | "detail" | "type" | "number1" | "number2";

export const defaultProfile: Profile = {
    username: "",
    id: '0',
    name: "Unknown",
    picture: null,
    
}

interface AppContext {
    initContext: () => void,

    content: Content[],
    addContent: (newContent: Content) => void,
    deleteContent: (content: Content) => void,
    updateContent: (updateContent: Content) => void,

    profile: Profile,
    updateProfile: (updatedProfile: Profile) => void
    
    user: firebase.User | null,
    authenticated: boolean;
    setUser: any,
    loadingAuthState: boolean
}

const AppContext = React.createContext<AppContext>({
    initContext: () => { },

    content: [],
    addContent: () => { },
    deleteContent: () => { },
    updateContent: () => { },


    profile: defaultProfile,
    updateProfile: () => { },

    user: null,
    authenticated: false,
    setUser: () => {},
    loadingAuthState: false,

});

export default AppContext