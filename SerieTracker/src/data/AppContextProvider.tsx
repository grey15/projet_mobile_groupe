import React, { useState, useEffect, useCallback, useRef } from 'react';
import AppContext, { Content, Profile, defaultProfile } from './app-context';

import { Filesystem, Plugins } from '@capacitor/core'
import { connect } from 'tls';
import firebase from '../firebase';

const { Storage } = Plugins;

const AppContextProvider: React.FC = (props) => {

    // artifact
    const [content, setContent] = useState<Content[]>([])
    const [profile, setProfile] = useState<Profile>(defaultProfile)
    const didMountRef = useRef(false);

    // auth
    const [user, setUser] = useState(null as firebase.User | null);
    const [loadingAuthState, setLoadingAuthState] = useState(true);

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user: any) => {
            setUser(user);
            setLoadingAuthState(false);
            let firebaseUser = user as firebase.User;
            const db = firebase.firestore();
            if (firebaseUser && firebaseUser.uid) {
                db.collection("Users").doc(firebaseUser.uid)
                    .onSnapshot(function (doc) {
                        const updatedProfile = doc.data() as Profile;
                        console.log("Current data: ", updatedProfile);
                        setProfile(updatedProfile)
                    });
                db.collection('Content').where("userId", "==", firebaseUser.uid)
                    //.orderBy("addDate")
                    .onSnapshot(function (querySnapshot) {
                        let listContent: Content[] = []
                        querySnapshot.forEach(function (doc) {
                            listContent.push(doc.data() as Content);
                        });
                        setContent(listContent)
                    });
            }
        });
    }, []);

    useEffect(() => {
        if (didMountRef.current) {
            console.log(profile)            
            Storage.set({ key: 'content', value: JSON.stringify(content) })            
        } else {
            didMountRef.current = true;
        }
    }, [content])

    const addContent = (newContent: Content) => {
        /**
         * setContent((prevState) => {
            let newListContent = [...prevState];
            newListContent.unshift(newContent)
            return newListContent
        })
         */
        
    }
    

    const deleteContent = (content: Content) => {
        if (content.picture.length > 0) {
            content.picture.forEach(picture => {
                const storage = firebase.storage();
                const storageRef = storage.ref();
                const imageRef = storageRef.child(picture);
                imageRef.delete()
            });
        }
        const db = firebase.firestore();
        const docRef = db.collection('Content').doc(content.id);
        db.runTransaction(function (transaction) {
            return transaction.get(docRef).then(function (doc) {
                if (!doc.exists) {
                    console.log("Fail to delete content")
                } else {
                    transaction.delete(docRef)
                }
            })
        })
    }

    const updateContent = (updateContent: Content) => {
        const db = firebase.firestore();
        const docRef = db.collection('Content').doc(updateContent.id);
        db.runTransaction(function (transaction) {
            return transaction.get(docRef).then(function (doc) {
                if (!doc.exists) {
                    console.log("Fail to update content")
                } else {
                    transaction.update(docRef, updateContent)
                }
            })
        })
    }
    


    const updateProfile = (updateProfile: Profile) => {
        const db = firebase.firestore();
        const docRef = db.collection('Users').doc(user?.uid)
        db.runTransaction(function (transaction){
            return transaction.get(docRef).then(function(doc){
                if (!doc.exists){
                    console.log("fail")
                }else{
                    
                }
            })
        })
    }
    /**
    const updateLanguage = (lng: string) => {
        i18n.changeLanguage(lng)
        let newProfile = { ...profile };
        newProfile.lng = lng;
        updateProfile(newProfile);
    }
    */

    const initContext = async () => {
        const profileData = await Storage.get({ key: 'profile' })
        const contentData = await Storage.get({ key: 'movie' })
        const storedProfile = profileData.value ? JSON.parse(profileData.value) : defaultProfile;
        const storedContent = contentData.value ? JSON.parse(contentData.value) : [];
        didMountRef.current = false;
        setProfile(storedProfile)
        setContent(storedContent)
    }

    return <AppContext.Provider value={{ 
        initContext, 
        
        content,
        addContent,
        deleteContent,
        updateContent,
        
        
        profile, 
        updateProfile,
        
        user,
        authenticated: user !== null,
        setUser,
        loadingAuthState
        
        }}>
        {props.children}
    </AppContext.Provider>
}

export default AppContextProvider