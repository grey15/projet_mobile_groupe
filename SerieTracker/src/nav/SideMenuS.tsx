import React from 'react';
import { IonMenu, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, IonRouterOutlet } from '@ionic/react';
import { ROUTE_HOME, ROUTE_LISTES, ROUTE_PAGES_BASE } from './Routes';
import { Route, Redirect } from 'react-router';
import List from '../pages/Lists';


const SideMenuS: React.FC = () => (
  <>
    <IonMenu side="start" menuId="first">
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>Start Menu</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          <IonItem>Menu Item</IonItem>
          <IonItem>Menu Item</IonItem>
          <IonItem>Menu Item</IonItem>
          <IonItem>Menu Item</IonItem>
          <IonItem>Menu Item</IonItem>
        </IonList>
      </IonContent>
    </IonMenu>

    <IonRouterOutlet>
    <Route path={ROUTE_LISTES} component={List} exact />
    <Redirect path={ROUTE_PAGES_BASE} exact to={ROUTE_HOME} />
    </IonRouterOutlet>
  </>
);

export default SideMenuS;