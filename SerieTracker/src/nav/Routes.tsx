export const ROUTE_PAGES_BASE = '/pages/';
export const ROUTE_LISTES = ROUTE_PAGES_BASE + 'lists/'; 
export const ROUTE_PROFILE =  ROUTE_PAGES_BASE + 'profile/';
export const ROUTE_HOME =  ROUTE_PAGES_BASE + 'Home/';
export const ROUTE_FICHE =  ROUTE_PAGES_BASE + 'Fiche/';


export const ROUTE_AUTH_BASE = "/auth/";
export const ROUTE_LOGIN = ROUTE_AUTH_BASE + 'Login/'
export const ROUTE_SIGN_UP = ROUTE_AUTH_BASE + 'Signup/'
export const ROUTE_AUTH_MAIL_CONFIRM = ROUTE_AUTH_BASE + 'confirm-mail/'
export const ROUTE_RESET_PSW = ROUTE_AUTH_BASE + 'reset-password/'
export const ROUTE_POLICY = '/privacy-policy/'