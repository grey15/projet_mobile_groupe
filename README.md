_**SérieTracker**_

L'application sera un Réseau social sur le thème des séries, films, et lectures.

L'application aura pour but de permettre l'enregistrement ainsi que le partage de la découverte ou du suivi des médias tel que : les séries, les films, ou les lectures diverses. Avec la possibilité de partager aux différents contacts de l'utilisateur.

L'application pourra également catégoriser les médias par "tag", permettant ainsi à l'utilisateur de créer des collections regroupé par ces tags.

**Apperçu de l'application** ====================================================================================

Les pages : 
    - connexion.
    - profil.
    - liste (série/film/lecture).
    - fil actualité.
    - fiche présentation (série/film/lecture).

Les fonctionnalités téléphone utilisées. : 
    - appareil photo.
    - notification vibrante.
    - accès aux contact.

**Descritif** ====================================================================================

Au lancement de l'application, suivant si l'utilisateur s'est déjà connecté, il sera dirigé vers l'accueil de l'application à partir duquel il pourra naviguer. Autrement, une authentification lui sera demandée afin de permettre une cohésion du contenu suivant l'utilisateur(en cas d'utilisateur multiple sur la même plateforme). De la l'utilisateur pourra donc accéder depuis l'accueil à une liste de contenu ayant comme état en cours, il aura également accès aux outils de navigation lui permettant dès lors : d'ajouter un nouveau contenu, d'accéder à la liste totale de ses contenus, de voir et modifier son profil et de consulter l'actualité sur les médias et catégories souhaités.

**Comportement** ====================================================================================

Depuis l'accueil  :
Sur le haut de l'écran, il sera visible : un bouton faisant apparaître un menu(décris ultérieurement.), une bar de recherche du contenu utilisateur.
En dessous, sera visible une liste de contenus lier à l'utilisateur et comportant l'état "en cours de visionnage" ou "en cours de lecture" et pour chaque objet, il sera affiché : l'image du lié au contenu et la progression(Série = saison, épisode ; film = temps ; lecture = tome, chapitre/scan, pages).
